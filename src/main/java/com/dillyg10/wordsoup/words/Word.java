package com.dillyg10.wordsoup.words;

/**
 * Represents a word that can be searched.
 *
 * It contains information about the raw word (with spaces)
 * the word that will be used for searching and whether or not it is
 * reversed. Additionally, it will contain a reference to another word
 * that links to the word string.
 */
public interface Word {
    /**
     * Represents a non-reversed, space-possible word
     * @return Raw word string
     */
    String getWord();

    /**
     * Represents the word that will be used for searching
     * (possibly reversed or with spaces)
     * @return The searchable word
     */
    String getSearchableWord();

    /**
     *
     * @return Whether or not this word object is a reverse version of the main word
     */
    boolean isReversed();

    /**
     * Get the word object that this is linked to
     * @return The other word object this is linked to
     */
    Word getOther();

    /**
     * Sets the other object this is linked to
     * @param other The other word object, getWord() on both should be the same
     */
    void setOther(Word other);
}
