package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.words.Word;
import com.dillyg10.wordsoup.words.WordMatch;

public class WSMatch implements WordMatch {

    private Word word;
    private int startingRow, startingCol, endingRow, endingCol;

    public WSMatch(Word word, int startingRow, int startingCol, int endingRow, int endingCol) {
        this.word = word;
        this.startingRow = startingRow;
        this.startingCol = startingCol;
        this.endingRow = endingRow;
        this.endingCol = endingCol;
    }

    @Override
    public Word getMatched() {
        return word;
    }

    @Override
    public int getStartingRow() {
        return startingRow;
    }

    @Override
    public int getStartingCol() {
        return startingCol;
    }

    @Override
    public int getEndingRow() {
        return endingRow;
    }

    @Override
    public int getEndingCol() {
        return endingCol;
    }

    @Override
    public boolean wasFoundInReverse() {
        return word.isReversed();
    }
}
