package com.dillyg10.wordsoup.charactergrid;

import java.util.Scanner;

/**
 * Represents a 2D collection of characters
 */
public interface CharacterGrid {

    /**
     * Gets the char at row/col
     * @param r Row
     * @param c Col
     * @return The character at that position
     */
    char charAt(int r, int c);

    /**
     * Ingests data from a scanner about the current contents of the
     * grid
     * @param characterStream The current stream to read from
     */
    void ingest(Scanner characterStream);

    /**
     * Gets the row dimension of this grid
     * @return row dimension
     */
    int getRows();

    /**
     * Gets the column dimension of this grid
     * @return colmun dimension
     */
    int getCols();

    /**
     * Performs a search using a character consumer. If the character
     * consumer returns true, the search ends
     * @param consumer The consumer to interpret the data
     */
    void search(CharacterConsumer consumer);
}
