package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.charactergrid.CharacterConsumer;
import com.dillyg10.wordsoup.charactergrid.CharacterGrid;
import com.dillyg10.wordsoup.charactergrid.CharacterGridWordFinder;
import com.dillyg10.wordsoup.misc.Direction;
import com.dillyg10.wordsoup.words.WordBank;
import com.dillyg10.wordsoup.words.WordMatch;
import com.dillyg10.wordsoup.words.WordSearchTrail;

import java.util.*;

public class WSWordFinder implements CharacterGridWordFinder, CharacterConsumer {

    private Map<String, WordMatch> matches;
    private WordBank currentBank;

    @Override
    public Map<String, WordMatch> search(CharacterGrid grid, WordBank wordBank) {
        matches = new HashMap<>();
        currentBank = wordBank;
        grid.search(this);
        return matches;
    }

    @Override
    public boolean consume(CharacterGrid grid, int row, int col, char current) {
        for (Direction direction : Direction.values()) {
            // a new query is performed since the last query could have removed words from the pool
            Set<WordSearchTrail> found =  currentBank.startSearch(current);
            // if there is no result, don't bother going through the other directions
            if (found.size() == 0) return false;
            // starts at the next position
            int workingRow = row + direction.getRowDirection();
            int workingCol = col + direction.getColDirection();

            while (found.size() > 0 && workingRow < grid.getRows() && workingCol >= 0 && workingCol < grid.getCols()) {
                Iterator<WordSearchTrail> i = found.iterator();
                // starts a new loop over the search results that can remove results in-place
                while (i.hasNext()) {

                    WordSearchTrail next = i.next();
                    WordSearchTrail.Result result = next.tryToAcceptAndProgress(grid.charAt(workingRow, workingCol));
                    switch (result) {
                        case FOUND:
                            matches.put(next.getWord().getWord(), new WSMatch(next.getWord(), row, col, workingRow, workingCol));
                            currentBank.markFound(next.getWord());
                            i.remove();
                            if (!currentBank.hasRemainingWords()) {
                                return true;
                            }
                            break;
                        case NOT_FOUND:
                            i.remove();
                            break;
                        case NEEDS_MORE:
                            // continue
                            break;
                    }
                }
                workingRow += direction.getRowDirection();
                workingCol += direction.getColDirection();
            }
        }
        return false;
    }
}
