package com.dillyg10.wordsoup.charactergrid;

import com.dillyg10.wordsoup.words.WordBank;
import com.dillyg10.wordsoup.words.WordMatch;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a word search given a grid and a bank to search from.
 * This will return a map of (display name of word found, the match object representing what was found)
 */
@FunctionalInterface
public interface CharacterGridWordFinder {
    Map<String, WordMatch> search(CharacterGrid grid, WordBank wordBank);
}
