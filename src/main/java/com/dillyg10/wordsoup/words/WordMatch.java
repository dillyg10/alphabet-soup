package com.dillyg10.wordsoup.words;

/**
 * Represents a found match for a word object
 *
 * start < end
 *
 * It is up to the interpreter of the match if the start/end coordinate order should be reversed
 */
public interface WordMatch {
    /**
     *
     * @return The word that was matched
     */
    Word getMatched();

    /**
     *
     * @return Starting row of the match
     */
    int getStartingRow();

    /**
     *
     * @return Starting column of the match
     */
    int getStartingCol();

    /**
     *
     * @return Ending row of the match
     */
    int getEndingRow();

    /**
     *
     * @return Ending col of the match
     */
    int getEndingCol();

    /**
     *
     * @return Whether or not the word found was the reversed version
     */
    boolean wasFoundInReverse();
}
