package com.dillyg10.wordsoup.misc;

/**
 * Represents a set of valid directions that can be traveled in on the grid
 */
public enum Direction {
    ROW(1, 0),
    COL(0, 1),
    DIAG_R(1, 1),
    DIAG_L(1, -1);

    private int rowDirection, colDirection;
    Direction(int rowDirection, int colDirection) {
        this.rowDirection = rowDirection;
        this.colDirection = colDirection;
    }

    public int getRowDirection() {
        return rowDirection;
    }

    public int getColDirection() {
        return colDirection;
    }
}
