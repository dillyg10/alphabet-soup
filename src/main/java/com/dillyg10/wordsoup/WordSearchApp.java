package com.dillyg10.wordsoup;

import com.dillyg10.wordsoup.game.WSGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

public class WordSearchApp {
    public static void main(String[] args) throws FileNotFoundException {
        // Reads input file, creates game, and tries to find words.
        File input = new File(args[0]);
        WSGame game = new WSGame();
        game.readInput(input);
        game.performSearchAndCacheResults();
        // Prints the result of the game to the console
        game.getResults().forEach(System.out::println);
    }
}
