package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.words.Word;
import com.dillyg10.wordsoup.words.WordBank;
import com.dillyg10.wordsoup.words.WordSearchTrail;

import java.util.*;
import java.util.stream.Collectors;

public class WSWordBank implements WordBank {

    // this buckets words based on their starting character.
    private Map<Character, Set<Word>> bank;
    private List<String> words;
    private int shortestWord;

    public WSWordBank() {
        bank = new HashMap<>();
        words = new ArrayList<>();
        shortestWord = Integer.MAX_VALUE;
    }

    @Override
    public List<String> getWords() {
        return words;
    }

    @Override
    public boolean hasRemainingWords() {
        return !bank.isEmpty();
    }

    @Override
    public void ingest(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String word = scanner.nextLine();
            // has tolerance for scanner not being on correct starting position
            if (!word.equals("")) {
                WSWord front = new WSWord(word, false);
                WSWord back = new WSWord(word, true);

                front.setOther(back);
                back.setOther(front);

                bank.computeIfAbsent(front.getSearchableWord().charAt(0), k -> new HashSet<>()).add(front);
                bank.computeIfAbsent(back.getSearchableWord().charAt(0), k -> new HashSet<>()).add(back);

                words.add(word);
                if (word.length() < shortestWord)
                    shortestWord = word.length();
            }
        }
    }

    @Override
    public int getShortedWordLength() {
        return shortestWord;
    }

    @Override
    public Set<WordSearchTrail> startSearch(char start) {
        Set<Word> validWords = bank.get(start);
        if (validWords == null) return new HashSet<>();
        return validWords.stream().map(WSSearchTrail::new).collect(Collectors.toSet());
    }

    @Override
    public void markFound(Word word) {
        char firstLetter = word.getSearchableWord().charAt(0), lastLetter = word.getSearchableWord().charAt(word.getSearchableWord().length() - 1);
       // this assumes that word has an other object and each is hashed based on its first letter
        for (char letter : new char[]{firstLetter, lastLetter}) {
            Set<Word> wordSet = bank.get(letter);
            if (letter == lastLetter)
                wordSet.remove(word.getOther());
            else
                wordSet.remove(word);
            if (wordSet.size() == 0)
                bank.remove(letter);
        }
    }
}
