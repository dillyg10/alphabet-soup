package com.dillyg10.wordsoup.game;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class WSGameTest {

    private static File sampleRoot = new File("samples");
    private static File inFolder = new File(sampleRoot, "in");
    private static File outFolder = new File(sampleRoot, "out");
    private static Set<IOPair> pairs = new HashSet<>();

    @BeforeAll
    public static void setup(){
        pairs = new HashSet<>();
        for (File file : inFolder.listFiles()) {
            pairs.add(new IOPair(file));
        }
    }

    @Test
    public void testGame() throws FileNotFoundException {
        for (IOPair pair : pairs) {
            WSGame game = new WSGame();
            game.readInput(pair.in);
            game.performSearchAndCacheResults();

            Scanner outScanner = new Scanner(pair.out);
            List<String> results = game.getResults();
            int i = 0;
            while (outScanner.hasNextLine()) {
                assertTrue(i < results.size());
                assertEquals(results.get(i), outScanner.nextLine());
                i++;
            }
        }
    }


    private static class IOPair {
        File in, out;

        public IOPair(File inFile) {
            this.in = inFile;
            this.out = new File(outFolder, inFile.getName());
        }
    }
}