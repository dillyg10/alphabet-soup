package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.words.Word;
import com.dillyg10.wordsoup.words.WordSearchTrail;

public class WSSearchTrail implements WordSearchTrail {

    private Word word;
    private int index;
    public WSSearchTrail(Word word){
        this.word = word;
        this.index = 1;
    }

    @Override
    public Word getWord() {
        return word;
    }

    @Override
    public Result tryToAcceptAndProgress(char input) {
        if (index >= getWord().getSearchableWord().length()) return Result.NOT_FOUND;
        if (getWord().getSearchableWord().charAt(index) == input) {
            if (++index >= getWord().getSearchableWord().length()) return Result.FOUND;
            return Result.NEEDS_MORE;
        }
        return Result.NOT_FOUND;
    }

    @Override
    public int remaining() {
        return word.getSearchableWord().length() - index;
    }
}
