package com.dillyg10.wordsoup.charactergrid;

/**
 * Represents an actor that can consume a character, its grid, and the
 * current cursor position and return whether or not the search should be
 * expected to end.
 */
@FunctionalInterface
public interface CharacterConsumer {
    boolean consume(CharacterGrid grid, int row, int col, char current);
}
