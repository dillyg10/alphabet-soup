package com.dillyg10.wordsoup.io;

import com.dillyg10.wordsoup.charactergrid.CharacterGrid;
import com.dillyg10.wordsoup.game.WSCharacterGrid;
import com.dillyg10.wordsoup.game.WSWordBank;
import com.dillyg10.wordsoup.words.WordBank;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WSFileReader {
    private File input;
    private Scanner scanner;
    public WSFileReader(File input) throws FileNotFoundException {
        this.input = input;
        this.scanner = new Scanner(input);
    }

    public CharacterGrid readCharacterGrid() {
        String dim = scanner.nextLine();
        String[] splitDim = dim.split("x");
        WSCharacterGrid grid = new WSCharacterGrid(Integer.parseInt(splitDim[0]), Integer.parseInt(splitDim[1]));
        grid.ingest(scanner);
        return grid;
    }

    public WordBank readWordBank(){
        WSWordBank bank = new WSWordBank();
        bank.ingest(scanner);
        return bank;
    }
}
