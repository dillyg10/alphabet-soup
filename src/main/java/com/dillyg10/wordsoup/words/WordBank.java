package com.dillyg10.wordsoup.words;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Stores the words in a way that can be searched given a character, and
 * updated to mark words that have been found
 */
public interface WordBank {
    /**
     * Gets the words in the bank, this indicates all words included found ones.
     * This is not updated
     * @return The collection of all words
     */
    List<String> getWords();

    /**
     *
     * @return Whether or not there are words that remain to be found in the bank
     */
    boolean hasRemainingWords();

    /**
     * Takes a scanner as input and ingests all words remaining in the scanner into the bank
     * @param scanner A scanner pointed at the section where word ingestion should start
     */
    void ingest(Scanner scanner);

    /**
     * Returns the shortest word in the bank
     * @return The shortest word in the bank
     */
    int getShortedWordLength();

    /**
     * Starts a search trail given the starting character. This will return all
     * words in the bank (in either forward or reverse order) that match that
     * starting character
     * @param start The starting character
     * @return The collection of words that match this starting query
     */
    Set<WordSearchTrail> startSearch(char start);

    /**
     * Will indicate that the word is found, and remove that word along with any
     * associated word objects from the bank
     * @param word the word to remove
     */
    void markFound(Word word);
}
