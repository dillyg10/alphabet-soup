package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.charactergrid.CharacterGrid;
import com.dillyg10.wordsoup.charactergrid.CharacterGridWordFinder;
import com.dillyg10.wordsoup.io.WSFileReader;
import com.dillyg10.wordsoup.words.WordBank;
import com.dillyg10.wordsoup.words.WordMatch;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WSGame {

    private CharacterGrid characterGrid;
    private WordBank bank;
    private CharacterGridWordFinder wordFinder;
    private Map<String, WordMatch> cachedResults;


    public WSGame() {
        this.wordFinder = new WSWordFinder();
    }

    public void readInput(File input) throws FileNotFoundException {
        WSFileReader fileReader = new WSFileReader(input);
        this.characterGrid = fileReader.readCharacterGrid();
        this.bank = fileReader.readWordBank();
    }

    public void performSearchAndCacheResults() {
        this.cachedResults = wordFinder.search(characterGrid, bank);
    }

    public List<String> getResults(){
        return bank.getWords().stream().map(word -> {
            WordMatch match = getCachedResults().get(word);
            if (match == null) return null;
            // formats results so WORD startR:startC endR:endC
            if (match.wasFoundInReverse())
                return String.format("%s %d:%d %d:%d", word, match.getEndingRow(), match.getEndingCol(), match.getStartingRow(), match.getStartingCol());
            return String.format("%s %d:%d %d:%d", word, match.getStartingRow(), match.getStartingCol(), match.getEndingRow(), match.getEndingCol());
        }).collect(Collectors.toList());
    }

    public Map<String, WordMatch> getCachedResults(){
        return cachedResults;
    }
}
