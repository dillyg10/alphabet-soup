package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.words.Word;

import java.util.Objects;

public class WSWord implements Word {

    private String word;
    private String wordNoSpaces;
    private boolean reversed;
    private Word other;

    public WSWord(String word, boolean reversed) {
        if (reversed){
            // this reverses a string and reomves all spaces
            StringBuilder reversedStr = new StringBuilder();
            for (int i = word.length() - 1; i >= 0; i--) {
                char letter = word.charAt(i);
                if (letter != ' ')
                    reversedStr.append(letter);
            }
            this.wordNoSpaces = reversedStr.toString();
        } else {
            // this just removes spaces
            this.wordNoSpaces = word.replace(" ","");
        }
        this.word = word;
        this.reversed = reversed;
    }

    // this object hashes/equals on wordNoSpaces field since that is the
    // unique identifier

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WSWord wordNode = (WSWord) o;
        return Objects.equals(wordNoSpaces, wordNode.wordNoSpaces);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordNoSpaces);
    }

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public String getSearchableWord() {
        return wordNoSpaces;
    }

    @Override
    public boolean isReversed() {
        return reversed;
    }

    @Override
    public Word getOther() {
        return other;
    }

    @Override
    public void setOther(Word other) {
        this.other = other;
    }


}
