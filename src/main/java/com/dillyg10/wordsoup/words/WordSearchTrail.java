package com.dillyg10.wordsoup.words;

/**
 * This represents the current state of a word being searched. It starts on
 * the second letter of the word and is expected to progress until the word
 * is found or not found.
 */
public interface WordSearchTrail {

    /**
     * The word this trail is using
     * @return the word
     */
    Word getWord();

    /**
     * This will take the input of the next character and return a result
     * based on the current position of the word.
     *
     * If NEEDS_MORE is returned, the index of the search will be incremented
     * @param input The current char
     * @return The result of using this character for input
     */
    Result tryToAcceptAndProgress(char input);

    /**
     *
     * @return The remaining chars available in the word to be searched
     */
    int remaining();

    enum Result {
        NOT_FOUND,
        NEEDS_MORE,
        FOUND
    }
}
