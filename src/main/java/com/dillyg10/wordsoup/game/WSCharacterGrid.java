package com.dillyg10.wordsoup.game;

import com.dillyg10.wordsoup.charactergrid.CharacterConsumer;
import com.dillyg10.wordsoup.charactergrid.CharacterGrid;

import java.util.Scanner;

public class WSCharacterGrid implements CharacterGrid {
    // use a 2darray as the store
    private char[][] grid;

    public WSCharacterGrid(int rows, int columns){
        this.grid = new char[rows][columns];
    }

    @Override
    public char charAt(int r, int c) {
        return grid[r][c];
    }

    @Override
    public void ingest(Scanner characterStream) {
        for (int r = 0; r < getRows(); r++) {
            for (int c = 0; c < getCols(); c++) {
                // reads the next char in the stream,
                // assumes the stream is pointed at the current readable character
                grid[r][c] = characterStream.next().charAt(0);
            }
        }
    }

    @Override
    public int getRows() {
        return grid.length;
    }

    @Override
    public int getCols() {
        return grid[0].length;
    }

    @Override
    public void search(CharacterConsumer consumer) {
        for (int r = 0; r < getRows(); r++) {
            for (int c = 0; c < getCols(); c++) {
                if (consumer.consume(this, r, c, charAt(r,c)))
                    return;
            }
        }
    }
}
